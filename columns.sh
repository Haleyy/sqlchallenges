#!/bin/bash

column=$1
file=$2

for word in $(awk -F, "{print \$$column}" $file )
do
  echo $word | wc -c
done | sort -nr | head -2 | tail -1
