/* Instructions:
Run the code below to create the procedure for challenge 2
Call the procedure in the following format: call query2('YYYY-MM-DD HH:MM:SS');
Make sure the input has single quotes around it
*/

use Haley;

delimiter //
create procedure query2(IN in_time datetime)
begin
	declare end_time datetime;
    declare curr_date VARCHAR(10);
    declare time_start VARCHAR(14);
    declare time_end VARCHAR(14);
    set end_time = date_add(in_time, interval 5 hour);
    set curr_date = date_format(in_time, "%d/%m/%Y");
    set time_start = date_format(in_time, "Start (%H:%i)");
    set time_end = date_format(end_time, " - End (%H:%i)");
	select sum(volume*close_price)/sum(volume) 'VWAP', curr_date 'date',
				concat(time_start, time_end) 'time'
    from dataset2 where dates between in_time and end_time;
end  //
delimiter ;
