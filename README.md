# SQL Challenges 2018

Follow the instructions below to run the SQL Challenges

1. createTable

    Run the file to create three tables, dataset1, dataset2, dataset3 and
    populate dataset2 and dataset3 with the given CSV files given by Steve.
    In order to populate the first file I imported the table using a gui.
    To do this right click on the table in MySQL and click 'Data Table Import
    Wizard' and follow the steps.

2. query1

    Run query1 to create the query1 procedure.
    Call the procedure in the following format: call query1('YYYY-MM-DD');

    Be sure to add single quotes around the input.

3. query2

    Run query2 to create the query2 procedure.
    Call the procedure in the following format: call query2('YYYY-MM-DD HH:MM:SS');
    
    Make sure the input has single quotes around it.

4. query3

    Run query3 to create the query3 procedure.
    Call the procedure in the following format: call query3;
