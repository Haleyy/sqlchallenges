/* Instructions: 
Run the code below to create the challenge 1 procedure.
Call the procedure in the following format: call query1('YYYY-MM-DD')
Be sure to add single quotes around the input.
*/

use Haley;

delimiter //
create procedure query1(IN in_date date) 
begin 
	select ticker 'Ticker', (((close_price - open_price)/ open_price)*100) 'Percentage Gain'
    from dataset1 where dates =  in_date
    order by (((close_price - open_price)/ open_price)*100) DESC  limit 5;
end  // 
delimiter ;

