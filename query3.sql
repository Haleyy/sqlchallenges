
/* Instructions:
Run the code below to create the procedure for challenge 3
Call the procedure in the following format: call query3;
*/

use Haley;

delimiter //
create procedure query3() 
begin 
	create temporary table mytbl
	select distinct date_format(dates, "%m/%d/%Y") 'dates', abs(close_price - open_price) 'ranges'
    from dataset3 
	order by abs(close_price - open_price) desc limit 3;
    
    create temporary table mytbl2
    select date_format(dates, "%m/%d/%Y") 'date1', max(high_price) 'high'
	from dataset3 
    group by date1;
    
    create temporary table mytbl3
    select date_format(dates, "%H:%i") 'time_of_high', max(high_price) 'high',
		date_format(dates, "%m/%d/%Y") 'date2'
	from dataset3 d3
    join mytbl2 t2 on t2.high = high_price and t2.date1 = date_format(dates, "%m/%d/%Y")
    group by date2;
    
    
    select m1.dates, m1.ranges, m3.time_of_high from mytbl m1
    inner join mytbl2 m2 on m2.date1 = m1.dates
    inner join mytbl3 m3 on m3.date2 = m1.dates;

    
    drop temporary table mytbl;
    drop temporary table mytbl2;
    drop temporary table mytbl3;
    
end // 
delimiter ;





